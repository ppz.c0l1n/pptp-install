#!/bin/bash
wan=$(ip -f inet -o addr show eth0|cut -d\  -f 7 | cut -d/ -f 1)
ppp1=$(/sbin/ip route | awk '/default/ { print $3 }')
ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

# Installing pptpd
echo "Installing PPTPD"
apt update && apt-get install pptpd -y

# edit DNS
echo "Setting Google DNS"
echo "ms-dns 8.8.8.8" >> /etc/ppp/pptpd-options
echo "ms-dns 8.8.4.4" >> /etc/ppp/pptpd-options

# Edit PPTP Configuration
echo "Editing PPTP Configuration"
remote="$ppp1"
remote+="00-200"
echo "localip $ppp1" >> /etc/pptpd.conf
echo "remoteip $remote" >> /etc/pptpd.conf

# Enabling IP forwarding in PPTP server
echo "Enabling IP forwarding in PPTP server"
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl -p

# Tinkering in Firewall
echo "Tinkering in Firewall"
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE && iptables-save
iptables --table nat --append POSTROUTING --out-interface ppp0 -j MASQUERADE
$("iptables -I INPUT -s $ip/8 -i ppp0 -j ACCEPT")
iptables --append FORWARD --in-interface eth0 -j ACCEPT

clear


[ -f /etc/ppp/chap-secrets ] && echo "$CLIENT * $PASS *" >> /etc/ppp/chap-secrets || touch /etc/ppp/chap-secrets && echo "$CLIENT * $PASS *" >> /etc/ppp/chap-secrets

# Restarting Service 
service pptpd restart

echo "All done!"